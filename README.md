# MAPA CONCEPTUAL NO.1: INDUSTRIA 4.0
```plantuml
@startmindmap
*[#LightCoral]  ***INDUSTRIA 4.0***
	*_ Es la
		*[#LightBlue] **Integración de tecnologías**
			*[#LightGreen] **Producción de datos**
			*[#LightGreen] **Software**
			*[#LightGreen] **Sensores**
			*_ con el propósito \n de
				*[#LightYellow] **Predecir**
				*[#LightYellow] **Controlar**
				*[#LightYellow] **Planear**
				*[#LightYellow] **Producir**
	*_ A lo largo de su 
		*[#LightBlue] **Historia**
			*_ existieron 3 versiones \n anteriores
				*[#LightGreen] **Industria 1.0**
					*_ Se caracteriza por 
						*[#LightYellow] **Producción mecánica**
						*[#LightYellow] **Equipos basados en energía** \n **a vapor e hidráulica**
				*[#LightGreen] **Industria 2.0**
					*_ Invensión de 
						*[#LightYellow] **Línea de ensamblaje**
						*[#LightYellow] **Energía eléctrica**
						*[#LightYellow] **Producción en masa**
				*[#LightGreen] **Industria 3.0**	
					*_ Aplicación de 
						*[#LightYellow] **Producción automatizada**
						*[#LightYellow] **Programmable logic controller**
	*_ Involucra muchas
		*[#LightBlue] **Tecnologias**
			*_ como por ejemplo 
				*[#LightGreen] **Robótica**
				*[#LightGreen] **Big Data**
				*[#LightGreen] **Cloud Computing**
				*[#LightGreen] **Sistema Autónomo**
				*[#LightGreen] **loT**
				*[#LightGreen] **Inteligencia Artificial**
	*_ involucra 
		*[#LightBlue] **Procesos de automatización** \n **en industrias**
			*_ que traen consigo 
				*[#LightGreen] **Cambios**
					*[#LightYellow] **Integracion de TICs en la industrias**
					*[#LightYellow] **Empresas de manufactura a empresas de TICs**
					*[#LightYellow] **Nuevos paradigmas y Tecnologias**
					*[#LightYellow] **Nuevas culturas digitales**
@endmindmap
```mindmap
```
# MAPA CONCEPTUAL NO.2: MÉXICO Y LA INDUSTRIA 4.0
```plantuml
@startmindmap
*[#LightCoral]  ***MÉXICO Y LA INDUSTRIA 4.0***
	*_ la cuarta revolución ayuda al 

		*[#LightBlue] **Desarrollo en México**
			*[#LightYellow] **México no se queda atrás en la digitalización** \n **de los sistemas fisicos y procesos de una planta**
				*_ Donde se involucran
					*[#LightGreen] **Startups**
						*_ como por ejemplo
							*[#LightYellow] **Sin llave**
							*[#LightYellow] **wearROBOT**
	*_ tiene
		*[#LightBlue] **Beneficios**
			*[#LightGreen] **Disponibilidad de la información** \n **en tiempo real**
				*_ disponer información en cualquier momento sobre**
					*[#LightYellow] **Procesos**
					*[#LightYellow] **Productos**
					*[#LightYellow] **Personas**
			*[#LightGreen] **Conectividad**
				*_ necesitamos poner en contexto la
					*[#LightYellow] **Importancia de transformar los datos en información**
			*[#LightGreen] **Auto organización**
				*[#LightYellow] **Las empresas buscan una organización inteligente**
			*[#LightGreen] **Reducción de costos**
				*_ consiste en
					*[#LightYellow] **Minimizar gastos**
					*[#LightYellow] **Incrementar ganancias**
	*_ aspectos a tomar en cuenta 
		*[#LightBlue] **Visibilidad**
			*[#LightYellow] **Las empresas tienen transparencia sobre el proceso de su valor agregrado**
			*[#LightYellow] **La gerencia toma decisiones sobre la base de datos**
		*[#LightBlue] **Transparencia**
			*[#LightYellow] **Las empresas entienden por qué pasan las cosas**
			*[#LightYellow] **El conocimiento es descubierto a través del reconocimiento**
		*[#LightBlue] **Predecibilidad**
			*[#LightYellow] **Las empresas saben qué pasará en el futuro**
			*[#LightYellow] **Las decisiones son basadas en el escenario futuro**
		*[#LightBlue] **Adaptabilidad**
			*[#LightYellow] **El sistema se controla a sí mismo**
			*[#LightYellow] **Es viable**			
@endmindmap
```
## Una breve reflexión 

Estoy totalmente convencida que este tema es de mucha importancia en nuestra carrera.
En lo personal, me ayuda a mejorar mi panorama sobre el futuro de la tecnología y abre mi
mente para ver futuras opciones de trabajo. Pero no sólo eso, me doy cuenta que
la industria 4.0 es un tema que involucra a todo el mundo.

## Una imagen
Imagen en formato JPG sobre el tema:

![Una imagen](imagenes/una-imagen.JPG)

### Despedida
Trabajo terminado. Muchas gracias por la atención  :blush: :smile: :blush:

## Material visual utilizado

[Industria 4 0 - Explicado Fácilmente](https://youtu.be/Qb7twp03c58)
[México: hacia la Industria 4.0](https://youtu.be/QfWjtWf8-6A)
[Conferencia Magistral "México en la Industria 4.0"](https://youtu.be/riMGgWLpQdM)

